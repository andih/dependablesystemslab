\chapter{Exercise 3 - Person tracking during tunnel construction}
% Description
In modern tunnel construction sites, the location of each person in the field has to be tracked to alarm them and provide informations to rescue workers in case of emergency. 
Because there is no GPS available in the mountain, the approximate location can be found on the basis of RSSI levels between fixed radio stations and wireless tags carried by each person in the tunnel. 
To achieve high availability, redundancy is provieded in the connection between user and radio station by using WiFi and LoRa and in the connection to the server by using an fiber optic network and WiFi as fallback solution. 
The schematical set-up is depicted in Figure \ref{fig:exercise3_setup}.
\vfill
\begin{figure}[h]
    \centering
    \includegraphics[width=\textwidth]{figures/Exercise3_setup.pdf}
    \caption{Exercise 3 set-up} 
    \label{fig:exercise3_setup}
\end{figure}
\vfill 

\section{Data Flow Graph}
Figure \ref{fig:ex3_dataflow} shows a \gls{DFG} for one tag which has connections to $n$ adjacent radio stations and uses at most $m$ radio stations to route the signal to the server. 
It can be observed that the connection between the tag and the radio stations can be modeled with a $2n$-redundant system with different failure- and repair rates. 
The model for the radio stations consists of $m$ double-redundant subsystems which are connected serially.

\vfill
\begin{figure}[ht]
    \centering
    \begin{tikzpicture}
        \node (tag) at (0,5) [draw, minimum width=3em] {Tag};
        \node (tag_lora_1) at (3,6) [draw, minimum width=3em] {Tag LoRa $1$};
        \node (tag_wifi_1) at (3,4) [draw, minimum width=3em] {Tag WiFi $1$};
        \node (tag_lora_n) at (3,2) [draw, minimum width=3em] {Tag LoRa $n$};
        \node (tag_wifi_n) at (3,0) [draw, minimum width=3em] {Tag WiFi $n$};

        \node (rs_fiber_m) at (7,6) [draw, minimum width=3em] {RS Fiber $m$};
        \node (rs_wifi_m) at (7,4) [draw, minimum width=3em] {RS WiFi $m$};
        \node (rs_fiber_1) at (11,6) [draw, minimum width=3em] {RS Fiber $1$};
        \node (rs_wifi_1) at (11,4) [draw, minimum width=3em] {RS WiFi $1$};

        \node (server) at (14,5) [draw, minimum width=3em] {Server};
        \node (cloud) at (16,5) [draw, minimum width=3em] {Cloud};

        \coordinate (c1) at (1,5);
        \fill (c1) circle (2pt);
        \draw [-] (tag) -- (c1);
        \draw [->] (c1) |- (tag_lora_1);
        \draw [->] (c1) |- (tag_wifi_1);

        \coordinate (c2) at (1,4);
        \fill (c2) circle (2pt);
        \coordinate (c3) at (1,2);
        \fill (c3) circle (2pt);
        \draw [dotted] (c2) -- (c3);
        \draw [->] (c3) -- (tag_lora_n);
        \draw [->] (c3) |- (tag_wifi_n);

        \coordinate (c4) at (4.6,5);
        \fill (c4) circle (2pt);
        \coordinate (c5) at (5.4,5);
        \fill (c5) circle (2pt);
        \draw [-] (tag_lora_1) -| (c4);
        \draw [-] (tag_wifi_1) -| (c4);
        \draw [-] (c4) -- (c5);
        \draw [->] (c5) |- (rs_fiber_m);
        \draw [->] (c5) |- (rs_wifi_m);

        \coordinate (c6) at (8.6,5);
        \fill (c6) circle (2pt);
        \coordinate (c7) at (9.4,5);
        \fill (c7) circle (2pt);
        \draw [-] (rs_fiber_m) -| (c6);
        \draw [-] (rs_wifi_m) -| (c6);
        \draw [dotted] (c6) -- (c7);
        \draw [->] (c7) |- (rs_fiber_1);
        \draw [->] (c7) |- (rs_wifi_1);

        \coordinate (c8) at (12.6,5);
        \fill (c8) circle (2pt);
        \draw [-] (rs_fiber_1) -| (c8);
        \draw [-] (rs_wifi_1) -| (c8);
        \draw [->] (c8) -- (server);
        \draw [->] (server) -- (cloud);

        \coordinate (c9) at (4.6,1);
        \fill (c9) circle (2pt);
        \draw [-] (tag_lora_n) -| (c9);
        \draw [-] (tag_wifi_n) -| (c9);

        \coordinate (c10) at (9,3.5);
        \draw [->] (c9) -| (c10);
    \end{tikzpicture}
    \caption{Data Flow Graph for one Tag}
    \label{fig:ex3_dataflow}
\end{figure}
\vfill

\section{Markov Model}
\label{sec:ex3_markov_Model}
Because of the multitude of different failure sources in the system, which results in a high number of states, a markov model is not realy feasable. 
However, Figure \ref{fig:model3} shows a simplified model for one tag, connected to one radio station, which is directly connected to the server.
This system consists basically of a serial connection of two double-redundant subsystems for the communication channels, the tag, the server and the cloud.
Starting at $s_0$, where all components are working, the second row in Figure \ref{fig:model3} represents a failure of either the LoRa ($s_1$)/WiFi ($s_2$) link to the tag or the fiber ($s_3$)/WiFi ($s_4$) link to the server. 
A failure in the second communication channel results in one simplified failure state ($s_{13}$) without repair possibility. 
States $s_5-s_{12}$ represents a failure in the other communication channel after a failure in the first channel.

\vfill
\begin{figure}[ht]
    \centering
    \begin{tikzpicture}%[
        %node distance = 7mm and 12mm, % for distance between nodes
        %]
        \node (s0) at (6,6) [circle, draw, minimum width=3em] {$s_0$};
        \node (s1) at (0,4) [circle, draw, minimum width=3em] {$s_1$};
        \node (s2) at (4,4) [circle, draw, minimum width=3em] {$s_2$};
        \node (s3) at (8,4) [circle, draw, minimum width=3em] {$s_3$};
        \node[label={[align=left, label distance=0.5cm]85:
            $\lambda_1, \mu_1:$ \textit{Tag LoRa Link}\\
            $\lambda_2, \mu_2:$ \textit{Tag WiFi Link}\\
            $\lambda_3, \mu_3:$ \textit{RS Fiber Link}\\
            $\lambda_4, \mu_4:$ \textit{RS WiFi Link}\\
            $\lambda_5:$ \textit{Tag failure rate}\\
            $\lambda_6:$ \textit{Server failure rate}\\ 
            $\lambda_7:$ \textit{Cloud failure rate}}] 
            (s4) at (12,4) [circle, draw, minimum width=3em] {$s_4$};

        \node (s5) at (1,2) [circle, draw, minimum width=3em] {$s_5$};
        \node (s6) at (3,2) [circle, draw, minimum width=3em] {$s_6$};
        \node (s7) at (5,2) [circle, draw, minimum width=3em] {$s_7$};
        \node (s8) at (7,2) [circle, draw, minimum width=3em] {$s_8$};
        \node (s9) at (9,2) [circle, draw, minimum width=3em] {$s_9$};
        \node (s10) at (11,2) [circle, draw, minimum width=3em] {$s_{10}$};
        \node (s11) at (13,2) [circle, draw, minimum width=3em] {$s_{11}$};
        \node (s12) at (15,2) [circle, draw, minimum width=3em] {$s_{12}$};

        \node (s13) at (6,0) [circle, draw=red, text=red, minimum width=3em] {$s_{13}$};
  
        \node[label={[align=center, label distance=-1cm]180:
            \textit{from all}\\
            \textit{states}}] 
            (all) at (3,-1) [circle, minimum width=3em] {};

        \draw[->, red] (all) to[out=10, in=205] node[above] {$\lambda_5$} (s13);
        \draw[->, red] (all) to[out=-30, in=220] node[above] {$\lambda_6$} (s13);
        \draw[->, red] (all) to[out=-70, in=235] node[below] {$\lambda_7$} (s13);

        \draw[->] (s0) to[out=160, in=45] node[above] {$\lambda_1$} (s1);
        \draw[<-] (s0) to[out=180, in=15] node[below] {$\mu_1$} (s1);
        \draw[->] (s0) to[out=205, in=65] node[above] {$\lambda_2$} (s2);
        \draw[<-] (s0) to[out=245, in=25] node[below] {$\mu_2$} (s2);
        \draw[->] (s0) to[out=335, in=115] node[above] {$\lambda_3$} (s3);
        \draw[<-] (s0) to[out=295, in=155] node[below] {$\mu_3$} (s3);
        \draw[->] (s0) to[out=20, in=135] node[above] {$\lambda_4$} (s4);
        \draw[<-] (s0) to[out=0, in=175] node[below] {$\mu_4$} (s4);

        \draw[->, red] (s1) to[out=260, in=190] node[below] {$\lambda_2$} (s13);
        \draw[->, red] (s2) to[out=270, in=145] node[left] {$\lambda_1$} (s13);
        \draw[->, red] (s3) to[out=270, in=35] node[right] {$\lambda_4$} (s13);
        \draw[->, red] (s4) to[out=280, in=350] node[below] {$\lambda_3$} (s13);

        \draw[->] (s1) to[out=280, in=135] node[below] {$\lambda_4$} (s5);
        \draw[<-] (s1) to[out=310, in=105] node[right] {$\mu_4$} (s5);
        \draw[->] (s1) to[out=330, in=145] node[above] {$\lambda_3$} (s6);
        \draw[<-] (s1) to[out=350, in=105] node[above] {$\mu_3$} (s6);
        \draw[->] (s2) to[out=280, in=135] node[below] {$\lambda_4$} (s7);
        \draw[<-] (s2) to[out=310, in=105] node[right] {$\mu_4$} (s7);
        \draw[->] (s2) to[out=330, in=145] node[above] {$\lambda_3$} (s8);
        \draw[<-] (s2) to[out=350, in=105] node[above] {$\mu_3$} (s8);
        \draw[->] (s3) to[out=280, in=135] node[below] {$\lambda_2$} (s9);
        \draw[<-] (s3) to[out=310, in=105] node[right] {$\mu_2$} (s9);
        \draw[->] (s3) to[out=330, in=145] node[above] {$\lambda_1$} (s10);
        \draw[<-] (s3) to[out=350, in=105] node[above] {$\mu_1$} (s10);
        \draw[->] (s4) to[out=285, in=135] node[below] {$\lambda_2$} (s11);
        \draw[<-] (s4) to[out=310, in=105] node[right] {$\mu_2$} (s11);
        \draw[->] (s4) to[out=330, in=145] node[above] {$\lambda_1$} (s12);
        \draw[<-] (s4) to[out=350, in=105] node[above] {$\mu_1$} (s12);

        \draw[->, red] (s5) to[out=315, in=175] node[above] {$\lambda_3$} (s13);
        \draw[->, red] (s6) to[out=315, in=160] node[left] {$\lambda_4$} (s13);
        \draw[->, red] (s7) to[out=280, in=130] node[right] {$\lambda_3$} (s13);
        \draw[->, red] (s8) to[out=260, in=50] node[left] {$\lambda_4$} (s13);
        \draw[->, red] (s9) to[out=225, in=20] node[right] {$\lambda_1$} (s13);
        \draw[->, red] (s10) to[out=225, in=05] node[above] {$\lambda_2$} (s13);
        \draw[->, red] (s11) to[out=240, in=335] node[below] {$\lambda_1$} (s13);
        \draw[->, red] (s12) to[out=240, in=320] node[below] {$\lambda_2$} (s13);
    \end{tikzpicture}
\caption{Markov Model for a System with one Tag and one Radio Station}\label{fig:model3}
\end{figure}

\section{PRISM Model}
In PRISM, the possibility of generating individual modules and combining them with boolean expressions, enables a more extensive model. 
The independence of the modules inherently modeles the states $s_5-s_{12}$ and individual failure states for each possible error. 
Because the position of the tag in the tunnel and the amount of connections from each tag to the radio stations vary over time and depends on the construction site, this parameters are controllable via the PRISM preprocessor system\footnote{\url{https://www.prismmodelchecker.org/prismpp/}}. 
To model the dependency of the failure rate of wireless connections on the distance between the tag and the radio station, a Gaussian function is used:
\begin{equation}
    r(x)=e^{-\frac{(x-\mu)^2}{2\sigma^2}}
    \label{eq:gauss}    
\end{equation}
With the parameters $\mu$ and $\sigma$, the position of the tag in the tunnel (in number of radio stations) and the size of the reception area can be defined (Figure \ref{fig:ex3_gauss}). 
The value for the failure rate of a connection can then be calculated with
\begin{equation}
    f_c(x)=\frac{f}{r(x)}
    \label{eq:failure_rate}    
\end{equation}
where $x$ describes the number of radio stations in the connection to the server and $f$ is the failure rate of the wireless connection within a small area around the sender. On e simplification is made by using the mean value of the amout of radio stations in the connection to the server used by each tag connection.
 
\begin{figure}
    \centerline{\includegraphics[scale=0.6]{figures/Exercise3_gauss}} 
    \caption{Model for the wireless connection failure rate}
    \label{fig:ex3_gauss}
\end{figure}
  
\lstinputlisting[language={Prism}, numbers=left, 
caption={Exercise 3 PRISM Model},label={lst_ex3_prism_model}]{../Exercise_3/model.prism.pp}

\section{Properties}
\lstinputlisting[language={Prism}, numbers=left, 
caption={Exercise 3 Model Properties},label={lst_ex3_props}]{../Exercise_3/properties.props}

\vfill
\section{Results}
Figure \ref{tab:ex3_results} depicts the reliability of the system. Table \ref{tab:ex3_results} shows the results with the input parameters $\mu$ (position of the tag), $\sigma_{LoRa}$ and $\sigma_{WiFi}$.
\vfill
\begin{table}
    \caption{Results}
    \begin{center}
    \begin{tabular}{ccc|cc}
    \toprule 
    \textbf{$\mu$} & \textbf{$\sigma_{LoRa}$} & \textbf{$\sigma_{WiFi}$} & \textbf{MTTF in days} & \textbf{Availability} \\
    \midrule
    $4$ & $0.4$ & $0.2$ & $176.7$ & $0.98969$ \\
    $4$ & $0.2$ & $0.2$ & $99.5$ & $0.98188$ \\
    \bottomrule
    \end{tabular}
    \label{tab:ex3_results}
    \end{center}
\end{table}
\vfill

\begin{figure}[h]
	\centering
	\includegraphics[scale=0.6]{figures/Exercise3_reliability}
	\caption{Exercise 3 Reliability Graph} 
	\label{fig:exercise3_reliability}
\end{figure}
