clc;
clear all;
close all;

font_size = 12;

x = [0.0 50.0 100.0 150.0 200.0 250.0 300.0 350.0 400.0 450.0 500.0 ];
y0 = [1.0 0.6085206203243867 0.3661866748440925 0.22035844739667598 0.13260407494667625 0.07979653559939026 0.04801878898687928 0.028896042647048148 0.017388636787325606 0.010463878843717245 0.0062967995591122206 ];
y1 = [1.0 0.7574112144648781 0.5696608685817071 0.42845079727500235 0.32224450645903757 0.24236510377061804 0.18228656299284374 0.13710055833447865 0.10311546165016161 0.07755474201197121 0.05833012733773735 ];

f0 = fit(x',y0','cubicinterp');
f1 = fit(x',y1','cubicinterp');
fx = 0:1:500;

plot(fx, f0(fx))
hold on
plot(fx, f1(fx))
set(gca,'FontUnits','points','FontWeight','normal','FontSize',font_size,'FontName','Times')
legend('\mu=4, \sigma_{LoRa}=0.2, \sigma_{WiFi}=0.2', '\mu=4, \sigma_{LoRa}=0.4, \sigma_{WiFi}=0.2');

xlabel('Time [days]',...
    'FontUnits','points','interpreter','latex','FontSize',font_size,'FontName','Times')
ylabel('Reliability',...
    'FontUnits','points','interpreter','latex','FontSize',font_size,'FontName','Times')
axis([0 500 0 1])
hold on
grid on

print -depsc Exercise3_reliability.eps