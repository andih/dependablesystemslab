clc;
clear all;
close all;

font_size = 12;

sigma = 0.5;
mu = 4;

x = -0:0.05:6;
y = exp(-(x-mu).^2/(2*sigma^2));

f = fit(x',y','poly3');
labels = {'Tag Position'};
plot(x,y,'o-','MarkerFaceColor',[0 .45 .74],'MarkerIndices',mu/0.05+1)
set(gca,'FontUnits','points','FontWeight','normal','FontSize',font_size,'FontName','Times')

text(mu+0.3,1,'Tag Position','VerticalAlignment','middle','HorizontalAlignment','left','Color',[0 0 0], 'FontUnits','points','interpreter','latex','FontSize',font_size,'FontName','Times')
text(0.3,0.95,'\mu=4','VerticalAlignment','middle','HorizontalAlignment','left','Color',[0 0 0], 'FontUnits','points','FontSize',font_size,'FontName','Times')
text(0.3,0.87,'\sigma=0.5','VerticalAlignment','middle','HorizontalAlignment','left','Color',[0 0 0], 'FontUnits','points','FontSize',font_size,'FontName','Times')

xlabel('x: Radio Stations',...
    'FontUnits','points','interpreter','latex','FontSize',font_size,'FontName','Times')
ylabel('r(x): Reliability of the Failure Rate',...
    'FontUnits','points','interpreter','latex','FontSize',font_size,'FontName','Times')
axis([0 6 0 1.1])
hold on
grid on

print -depsc Exercise3_gauss.eps