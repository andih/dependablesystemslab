#const TAG_POSITION_10#
#const SIGMA_LORA_10#
#const SIGMA_WIFI_10#

#const RS_RECEPTION_AREA_LORA_MIN = max(1, round(TAG_POSITION_10/10 - 2*SIGMA_LORA_10/10))#
#const RS_RECEPTION_AREA_LORA_MAX = round(TAG_POSITION_10/10 + 2*SIGMA_LORA_10/10)#
#const RS_RECEPTION_AREA_WIFI_MIN = max(1, round(TAG_POSITION_10/10 - 2*SIGMA_WIFI_10/10))#
#const RS_RECEPTION_AREA_WIFI_MAX = round(TAG_POSITION_10/10 + 2*SIGMA_WIFI_10/10)#

ctmc

// -------- Failure and Repair Rates --------
// all rates are given in occurences per day

// Connections between Tag and Radio Station
const double tag_lora_rate_failure = 1/10;
const double tag_wifi_rate_failure = 1/5;
const double tag_lora_rate_repair  = 1/1;
const double tag_wifi_rate_repair  = 1/1;

// Connections between Radio Stations and between the first Radio Station and the Server
const double rs_fiber_rate_failure = 1/10000;
const double rs_wifi_rate_failure  = 1/1000;
const double rs_fiber_rate_repair  = 1/10;
const double rs_wifi_rate_repair   = 1/5;

const double server_rate_failure   = 1/10000;
const double server_rate_repair    = 1/10;

const double cloud_rate_failure    = 1/10000;
const double cloud_rate_repair     = 1/10;

const double tag_rate_failure      = 1/10000;
const double tag_rate_repair       = 1/10;

// Placeholder for Template Models
const double a0 = 1/100000;
const double a1 = 1/100000;
const double b0 = 1/100000;
const double b1 = 1/100000;

// ---------------- Modules -----------------
// Main module
module main
	s: [0..1] init 0;

	[] 	(s=0) & 
		(
			(
				// Tag LoRa connections (parallel)
				#& i=RS_RECEPTION_AREA_LORA_MIN:RS_RECEPTION_AREA_LORA_MAX# (tcl#i#=1) #end# & 
				// Tag WiFi connections (parallel)
				#& i=RS_RECEPTION_AREA_WIFI_MIN:RS_RECEPTION_AREA_WIFI_MAX# (tcw#i#=1) #end#   
			) |
			// radio stations (serial)
			#| i=1:round(TAG_POSITION_10/10)# (rs#i#=3) #end# | 
			(s_server = 1) |
			(s_cloud  = 1) |
			(s_tag    = 1)
		) -> (s'=1);

	[] 	(s=1) & 
		!(
			(
				// Tag LoRa connections (parallel)
				#& i=RS_RECEPTION_AREA_LORA_MIN:RS_RECEPTION_AREA_LORA_MAX# (tcl#i#=1) #end# & 
				// Tag WiFi connections (parallel)
				#& i=RS_RECEPTION_AREA_WIFI_MIN:RS_RECEPTION_AREA_WIFI_MAX# (tcw#i#=1) #end#   
			) |
			// radio stations (serial)
			#| i=1:round(TAG_POSITION_10/10)# (rs#i#=3) #end# | 
			(s_server = 1) |
			(s_cloud  = 1) |
			(s_tag    = 1)
		) -> (s'=0);
endmodule

// Template Module for a double-redundant system
// a0, a1: failure rates
// b0, b1: repair rates
module RedundantSystem
	r: [0..3] init 0;

	[] r=0 -> a0: (r'=1);
	[] r=0 -> a1: (r'=2);
	[] r=1 -> a1: (r'=3);
	[] r=2 -> a0: (r'=3);

	[] r=1 -> b0: (r'=0);
	[] r=2 -> b1: (r'=0);
	[] r=3 -> b0: (r'=1);
	[] r=3 -> b1: (r'=2);
endmodule

// Template Module for a single stand-alone component
// a0: failure rate
// b0: repair rate
module StandAloneComponent
	c: [0..1] init 0;

	[] c=0 -> a0: (c'=1);
	[] c=1 -> b0: (c'=0); 
endmodule

module Server = StandAloneComponent [c=s_server, a0=server_rate_failure, b0=server_rate_repair] endmodule
module Cloud  = StandAloneComponent [c=s_cloud,  a0=cloud_rate_failure,  b0=cloud_rate_repair]  endmodule
module Tag    = StandAloneComponent [c=s_tag,    a0=tag_rate_failure,    b0=tag_rate_repair]    endmodule

// Tag LoRa connections
#for i=RS_RECEPTION_AREA_LORA_MIN:RS_RECEPTION_AREA_LORA_MAX#
const double tag_#i#_lora_rate_failure = tag_lora_rate_failure / (pow(2.718281828459, -pow(#i#-#TAG_POSITION_10/10#, 2)/2/pow(#SIGMA_LORA_10/10#,2)));
module TagLoRaConnection#i# = StandAloneComponent [c=tcl#i#, a0=tag_#i#_lora_rate_failure, b0=tag_lora_rate_repair] endmodule

#end#

// Tag WiFi connections
#for i=RS_RECEPTION_AREA_WIFI_MIN:RS_RECEPTION_AREA_WIFI_MAX#
const double tag_#i#_wifi_rate_failure = tag_wifi_rate_failure / (pow(2.718281828459, -pow(#i#-#TAG_POSITION_10/10#, 2)/2/pow(#SIGMA_WIFI_10/10#,2)));
module TagWiFiConnection#i# = StandAloneComponent [c=tcw#i#, a0=tag_#i#_wifi_rate_failure, b0=tag_wifi_rate_repair] endmodule

#end#

// Radio Stations
#for i=1:round(TAG_POSITION_10/10)#
module RadioStation#i# = RedundantSystem [r=rs#i#, a0=rs_fiber_rate_failure, a1=rs_wifi_rate_failure, b0=rs_fiber_rate_repair, b1=rs_wifi_rate_repair] endmodule
#end#

// ------------- Reward System --------------
rewards
	s=0: 1;
endrewards
